import validator from 'validator'

export const email_validator_adapter = (email: string): boolean =>
  validator.isEmail(email)
