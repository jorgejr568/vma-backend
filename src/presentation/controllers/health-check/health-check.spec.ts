import { health_check_controller } from './health-check'
import { ok_response } from '../../responses/ok-response'

const makeSut = () => health_check_controller

describe('HealthCheck Controller test', () => {
  test('Must return an ok_response with status "pai ta online"', async () => {
    const sut = makeSut()
    const response = await sut({})

    expect(response).toEqual(
      ok_response({
        status: 'pai ta online',
      })
    )
  })
})
