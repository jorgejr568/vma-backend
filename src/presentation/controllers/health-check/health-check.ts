import { ok_response } from '../../responses/ok-response'
import { HttpRequest, HttpResponse } from '../../protocols/http'

export const health_check_controller = (_: HttpRequest): HttpResponse => {
  return ok_response({
    status: 'pai ta online',
  })
}
