import { ListUserRepository } from '../../../domain/user'
import { HttpRequest, HttpResponse } from '../../protocols/http'
import { ok_response } from '../../responses/ok-response'

export const user_list_controller = (
  listUserRepository: ListUserRepository
) => async (_: HttpRequest): Promise<HttpResponse> => {
  const users = await listUserRepository.list()
  return ok_response(users)
}
