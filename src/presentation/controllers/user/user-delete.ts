import { DeleteUserRepository } from '../../../domain/user'
import { HttpRequest, HttpResponse } from '../../protocols/http'
import { ok_response } from '../../responses/ok-response'

export const user_delete_controller = (
  deleteUserRepository: DeleteUserRepository
) => async (request: HttpRequest): Promise<HttpResponse> => {
  const { uuid } = request.route_params

  await deleteUserRepository.delete(uuid)
  return ok_response({
    deleted: true,
  })
}
