import { TUserUpdateRequest, UpdateUserRepository } from '../../../domain/user'
import { HttpRequest, HttpResponse } from '../../protocols/http'
import { ok_response } from '../../responses/ok-response'
import { generate_hash } from '../../../infra/hash/hash-lib-adapter'
import { IValidatorComposite } from '../../validator'
import { bad_request_response } from '../../responses/bad-request-response'
import { remove_undefined } from '../../util/remove-undefined'

export const user_update_controller = (
  updateUserRepository: UpdateUserRepository,
  validator: IValidatorComposite
) => async (request: HttpRequest): Promise<HttpResponse> => {
  const { uuid } = request.route_params
  const { name, email, password, address, age } = request.body
  const requestUser: TUserUpdateRequest = remove_undefined<TUserUpdateRequest>(
    {
      password:
        typeof password === 'string'
          ? await generate_hash(password)
          : undefined,
      name,
      email,
      age,
      address: {
        raw: address?.raw,
      },
    },
    true
  )

  const error = await validator.validate(request.body)
  if (error instanceof Error) return bad_request_response(error)

  console.log({ requestUser })
  const user = await updateUserRepository.update(uuid, requestUser)
  return ok_response(user)
}
