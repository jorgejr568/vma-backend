import { CreateUserRepository, TUserCreateRequest } from '../../../domain/user'
import { HttpRequest, HttpResponse } from '../../protocols/http'
import { ok_response } from '../../responses/ok-response'
import { generate_uuid } from '../../../infra/uuid/uuid-lib-adapter'
import { generate_hash } from '../../../infra/hash/hash-lib-adapter'
import { IValidatorComposite } from '../../validator'
import { bad_request_response } from '../../responses/bad-request-response'

export const user_store_controller = (
  createUserRepository: CreateUserRepository,
  validator: IValidatorComposite
) => async (request: HttpRequest): Promise<HttpResponse> => {
  const { name, email, password, address, age } = request.body
  const requestUser: TUserCreateRequest = {
    uuid: generate_uuid(),
    password:
      typeof password === 'string' ? await generate_hash(password) : null,
    name,
    email,
    age,
    address: {
      raw: address?.raw,
    },
  }

  const error = await validator.validate(request.body)
  if (error instanceof Error) return bad_request_response(error)

  const user = await createUserRepository.store(requestUser)
  return ok_response(user)
}
