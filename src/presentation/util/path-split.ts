import { pathOr } from 'ramda'

export const path_split = (field_name: string, obj: any) =>
  pathOr(null, field_name.split('.'), obj)
