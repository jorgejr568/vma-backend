import { isEmpty } from 'ramda'

export const remove_undefined = <T = any, R = T>(
  obj: any,
  empty_objects = false
): R => {
  const clone = { ...obj }
  Object.keys(clone).forEach((key) => {
    if (typeof clone[key] === 'object')
      clone[key] = remove_undefined(clone[key], empty_objects)

    if (clone[key] === undefined || (empty_objects && isEmpty(clone[key]))) {
      console.log({
        clone,
        key,
      })
      delete clone[key]
    }
  })

  return clone
}
