export class ValidationError extends Error {
  constructor(message: string) {
    super(message)
  }

  toString() {
    return this.message
  }
}
