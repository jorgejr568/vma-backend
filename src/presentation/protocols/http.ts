export interface HttpRequest {
  body?: any
  route_params?: any
  query_params?: any
}

export interface HttpResponse {
  body: any
  status_code: number
}
