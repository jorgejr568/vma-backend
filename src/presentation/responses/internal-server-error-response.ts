import { HttpResponse } from '../protocols/http'

export const internal_server_error_response = (e): HttpResponse => ({
  body: {
    error: 'Internal server error',
    stack: e,
  },
  status_code: 500,
})
