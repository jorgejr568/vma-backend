import { HttpResponse } from '../protocols/http'

export const not_found_request_response = (e: Error): HttpResponse => ({
  body: {
    error: 'Not found',
  },
  status_code: 404,
})
