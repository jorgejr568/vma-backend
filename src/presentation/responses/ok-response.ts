import { HttpResponse } from '../protocols/http'

export const ok_response = (body): HttpResponse => ({
  body,
  status_code: 200,
})
