import { HttpResponse } from '../protocols/http'

export const bad_request_response = (e: Error): HttpResponse => ({
  body: {
    error: e.message,
  },
  status_code: 400,
})
