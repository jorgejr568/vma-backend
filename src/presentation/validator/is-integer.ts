import { IValidatorComposite } from './composite'
import { path_split } from '../util/path-split'
import { ValidationError } from '../errors'

export class IsIntegerValidator implements IValidatorComposite {
  private readonly field_name: string
  private readonly nullable: boolean

  constructor(field_name: string, nullable = false) {
    this.field_name = field_name
    this.nullable = nullable
  }

  async validate(input: any): Promise<Error | void> {
    const value = path_split(this.field_name, input)

    if (this.nullable && !value) return

    if (!Number.isInteger(value))
      return new ValidationError(`${this.field_name} must to be an integer`)
  }
}
