import { IValidatorComposite } from './composite'
import { ValidationError } from '../errors'
import { path_split } from '../util/path-split'

export class RequiredValidator implements IValidatorComposite {
  private readonly field_name

  constructor(field_name) {
    this.field_name = field_name
  }

  async validate(input: any): Promise<Error | void> {
    const value = path_split(this.field_name, input)

    if (!value)
      return new ValidationError(`${this.field_name} is a required field`)
  }
}
