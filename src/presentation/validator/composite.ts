export interface IValidatorComposite {
  validate(input: any): Promise<Error | void>
}

export class ValidatorComposite implements IValidatorComposite {
  private readonly validators: IValidatorComposite[]

  constructor(validators: IValidatorComposite[]) {
    this.validators = validators
  }

  async validate(input: any): Promise<Error | void> {
    for (const validator of this.validators) {
      const error = await validator.validate(input)
      if (error) return error
    }
  }
}
