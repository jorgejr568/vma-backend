import { IValidatorComposite } from './composite'
import { email_validator_adapter } from '../adapters/email-validator-adapter'
import { path_split } from '../util/path-split'
import { ValidationError } from '../errors'

export class IsEmailValidator implements IValidatorComposite {
  private readonly field_name: string
  private nullable: boolean
  private email_validator: (email: string) => boolean

  constructor(
    field_name: string,
    nullable = false,
    email_validator: (email: string) => boolean = email_validator_adapter
  ) {
    this.nullable = nullable
    this.field_name = field_name
    this.email_validator = email_validator
  }

  async validate(input: any): Promise<Error | void> {
    const value = path_split(this.field_name, input)

    if (this.nullable && !value) return

    if (!this.email_validator(value))
      return new ValidationError(
        `${this.field_name} must to be an valid e-mail`
      )
  }
}
