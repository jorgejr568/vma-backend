import { Request as ExpressRequest, Response as ExpressResponse } from 'express'
import { HttpRequest, HttpResponse } from '../../presentation/protocols/http'
import { internal_server_error_response } from '../../presentation/responses/internal-server-error-response'

export const express_request_adapter = (
  express_request: ExpressRequest
): HttpRequest => ({
  body: express_request.body,
  route_params: express_request.params,
  query_params: express_request.query,
})

export const express_response_adapter = (
  http_response: HttpResponse,
  express_response: ExpressResponse
) => express_response.json(http_response.body).status(http_response.status_code)

export const express_adapter = (controller: Function) => async (
  express_request: ExpressRequest,
  express_response: ExpressResponse
) => {
  let http_response
  try {
    http_response = await controller(express_request_adapter(express_request))
  } catch (e) {
    http_response = internal_server_error_response(e)
  }

  return express_response_adapter(http_response, express_response)
}
