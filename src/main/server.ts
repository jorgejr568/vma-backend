import { App } from './boot/app'
import { MongoHelper } from '../infra/mongodb/mongo-helper'
console.log(process.env.MONGO_URL)
MongoHelper.connect(process.env.MONGO_URL)
  .then(() => {
    console.log('Connected to database')

    return App.listen(3000, () => {
      console.log('Sup dog! Listen to :3000')
    })
  })
  .catch(console.error)
