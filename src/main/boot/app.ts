import path from 'path'
import Express from 'express'
import { BootRoutes } from '../routes'

require('dotenv').config({
  path: path.resolve(__dirname, '..', '..', '..', '.env'),
})

export const App = Express()

App.use(Express.json())
BootRoutes(App)
