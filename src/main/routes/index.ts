import { Express, Router } from 'express'
import { health_check_controller } from '../../presentation/controllers'
import { express_adapter as e } from '../adapters/express-adapter'
import {
  user_list_controller_factory,
  user_store_controller_factory,
} from '../factories'
import { user_update_controller_factory } from '../factories/controllers/user-update-controller'
import { user_delete_controller_factory } from '../factories/controllers/user-delete-controller'

export const BootRoutes = (app: Express) => {
  const router = Router()
  app.use('/api', router)

  /**
   * Health check
   */
  router.get('/health', e(health_check_controller))

  /**
   * User resource
   */

  router.get('/user', e(user_list_controller_factory))
  router.post('/user', e(user_store_controller_factory))
  router.put('/user/:uuid', e(user_update_controller_factory))
  router.delete('/user/:uuid', e(user_delete_controller_factory))
}
