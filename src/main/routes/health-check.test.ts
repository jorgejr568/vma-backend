import request from 'supertest'
import { App } from '../boot/app'

describe('HealthCheck Routes', () => {
  test('Should return an ok_response with status "pai ta online"', async () => {
    const response = await request(App).get('/api/health').send()

    expect(response.status).toBe(200)
    expect(response.body).toEqual({
      status: 'pai ta online',
    })
  })
})
