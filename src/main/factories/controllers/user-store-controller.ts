import { mongo_user_repository_factory } from '../repositories/mongo-user-repository'
import { user_store_controller } from '../../../presentation/controllers/user/user-store'
import {
  RequiredValidator,
  ValidatorComposite,
} from '../../../presentation/validator'
import { IsEmailValidator } from '../../../presentation/validator/is-email'
import { IsIntegerValidator } from '../../../presentation/validator/is-integer'

export const user_store_controller_factory = user_store_controller(
  mongo_user_repository_factory(),
  new ValidatorComposite([
    new RequiredValidator('name'),
    new RequiredValidator('age'),
    new RequiredValidator('password'),
    new RequiredValidator('email'),
    new RequiredValidator('address.raw'),
    new IsEmailValidator('email'),
    new IsIntegerValidator('age'),
  ])
)
