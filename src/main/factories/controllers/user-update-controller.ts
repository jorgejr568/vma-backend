import { mongo_user_repository_factory } from '../repositories/mongo-user-repository'
import { ValidatorComposite } from '../../../presentation/validator'
import { IsEmailValidator } from '../../../presentation/validator/is-email'
import { IsIntegerValidator } from '../../../presentation/validator/is-integer'
import { user_update_controller } from '../../../presentation/controllers/user/user-update'

export const user_update_controller_factory = user_update_controller(
  mongo_user_repository_factory(),
  new ValidatorComposite([
    new IsEmailValidator('email', true),
    new IsIntegerValidator('age', true),
  ])
)
