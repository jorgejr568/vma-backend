import { user_list_controller } from '../../../presentation/controllers/user/user-list'
import { mongo_user_repository_factory } from '../repositories/mongo-user-repository'

export const user_list_controller_factory = user_list_controller(
  mongo_user_repository_factory()
)
