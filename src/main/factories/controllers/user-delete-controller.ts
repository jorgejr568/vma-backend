import { mongo_user_repository_factory } from '../repositories/'
import { user_delete_controller } from '../../../presentation/controllers/user/user-delete'

export const user_delete_controller_factory = user_delete_controller(
  mongo_user_repository_factory()
)
