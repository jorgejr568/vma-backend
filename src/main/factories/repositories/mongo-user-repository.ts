import { MongoUserRepository } from '../../../data/repositories/user-repositories/mongo-user-repository'

export const mongo_user_repository_factory = () => new MongoUserRepository()
