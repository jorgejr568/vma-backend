export interface DeleteUserRepository {
  delete(uuid: string): Promise<void>
}
