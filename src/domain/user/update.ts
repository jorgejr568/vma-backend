import { IUser } from './model'
import { TUserCreateRequest } from './create'

export type TUserUpdateRequest = Omit<TUserCreateRequest, 'id' | 'uuid'>

export interface UpdateUserRepository {
  update(id: string | number, request: TUserUpdateRequest): Promise<IUser>
}
