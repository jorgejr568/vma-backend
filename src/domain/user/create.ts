import { IUser } from './model'

export type TUserCreateRequest = Omit<IUser, 'id'>

export interface CreateUserRepository {
  store(request: TUserCreateRequest): Promise<IUser>
}
