import { IUser } from './model'

export type TUserListRequest = Partial<IUser>

export interface ListUserRepository {
  list(request?: TUserListRequest): Promise<IUser[]>
}
