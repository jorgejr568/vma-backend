export interface IUser {
  id: string | number
  uuid: string
  name: string
  email: string
  age: number
  password: string
  address: IUserAddress
}

export interface IUserAddress {
  raw: String
}
