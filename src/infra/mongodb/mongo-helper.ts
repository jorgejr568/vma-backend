import { Collection, MongoClient } from 'mongodb'

export const MongoHelper = {
  uri: null as String,
  client: null as MongoClient,
  async connect(uri): Promise<void> {
    this.uri = uri
    this.client = await MongoClient.connect(this.uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  },

  async disconnect(): Promise<void> {
    await this.client?.close()
  },

  async getCollection(name: string): Promise<Collection> {
    if (!this.client?.isConnected()) await this.connect(this.uri)
    return new Promise((resolve) => resolve(this.client.db().collection(name)))
  },

  map_IdtoId(collection: any): any {
    const { _id, ...collectionWithoutId } = collection
    return Object.assign(collectionWithoutId, { id: _id })
  },
}
