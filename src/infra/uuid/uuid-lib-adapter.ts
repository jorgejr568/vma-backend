import { v4 } from 'uuid'

export const generate_uuid = () => v4()
