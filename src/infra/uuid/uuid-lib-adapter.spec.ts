jest.mock('uuid')

const uuid = require('uuid')
const { validate, v4 } = jest.requireActual('uuid')
const { generate_uuid } = require('./uuid-lib-adapter')

describe('UUID lib adapter', () => {
  test('Must return v4 method from uuid', () => {
    uuid.v4.mockImplementationOnce(() => 'any_uuid')
    const response = generate_uuid()
    expect(response).toBe('any_uuid')
  })

  test('Must return an valid UUID', () => {
    uuid.v4.mockImplementationOnce(v4)
    const response = generate_uuid()
    expect(validate(response)).toBeTruthy()
  })
})
