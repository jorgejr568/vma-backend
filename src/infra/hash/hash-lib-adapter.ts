import { hash } from 'bcrypt'

export const generate_hash = (string: string): Promise<string> =>
  hash(string, process.env.SALT_ROUNDS || 10)
