jest.mock('bcrypt')

const bcrypt = require('bcrypt')
const { compare, hash } = jest.requireActual('bcrypt')
const { generate_hash } = require('./hash-lib-adapter')

describe('Hash lib adapter', () => {
  test('Must return hash method from bcrypt', async () => {
    bcrypt.hash.mockImplementationOnce(() => 'hashed_text')
    const response = await generate_hash('any_text')
    expect(response).toBe('hashed_text')
  })

  test('Must return be comparable with original text', async () => {
    const original_text = 'secret'
    bcrypt.hash.mockImplementationOnce(hash)
    const response = await generate_hash(original_text)
    await expect(compare('invalid_secret', response)).resolves.toBeFalsy()
    await expect(compare(original_text, response)).resolves.toBeTruthy()
  })
})
