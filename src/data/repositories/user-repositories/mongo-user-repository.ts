import {
  CreateUserRepository,
  DeleteUserRepository,
  IUser,
  ListUserRepository,
  TUserCreateRequest,
  TUserListRequest,
  TUserUpdateRequest,
  UpdateUserRepository,
} from '../../../domain'

import { MongoHelper } from '../../../infra/mongodb/mongo-helper'
import { generate_hash } from '../../../infra/hash/hash-lib-adapter'

export class MongoUserRepository
  implements
    CreateUserRepository,
    UpdateUserRepository,
    ListUserRepository,
    DeleteUserRepository {
  static collection() {
    return MongoHelper.getCollection('users')
  }

  static parse_user(row: any): IUser {
    const {
      _id,
      uuid,
      name,
      age,
      email,
      password,
      address: { raw },
    } = row

    return {
      id: _id.toString(),
      uuid,
      name,
      age,
      email,
      password,
      address: { raw },
    }
  }

  async delete(uuid: string): Promise<void> {
    const collection = await MongoUserRepository.collection()
    await collection.deleteOne({ uuid })
  }

  async list(request?: TUserListRequest): Promise<IUser[]> {
    const collection = await MongoUserRepository.collection()
    const rows = await collection.find({}).toArray()
    return rows.map(MongoUserRepository.parse_user)
  }

  async store(request: TUserCreateRequest): Promise<IUser> {
    const collection = await MongoUserRepository.collection()
    const row = await collection.insertOne(
      Object.assign({
        ...request,
        password: await generate_hash(request.password),
      })
    )
    return MongoUserRepository.parse_user(row.ops[0])
  }

  async update(uuid: string, request: TUserUpdateRequest): Promise<IUser> {
    const collection = await MongoUserRepository.collection()
    await collection.updateOne({ uuid }, { $set: request })
    const row = await collection.findOne({ uuid })
    return MongoUserRepository.parse_user(row)
  }
}
