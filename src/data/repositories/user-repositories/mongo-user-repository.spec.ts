import { MongoUserRepository } from './mongo-user-repository'
import { IUser } from '../../../domain/user'
import { generate_uuid } from '../../../infra/uuid/uuid-lib-adapter'
import { MongoHelper } from '../../../infra/mongodb/mongo-helper'

const classSut = MongoUserRepository
const makeSut = () => new MongoUserRepository()

const makeFakeUser = async (): Promise<Omit<IUser, 'id'> & { _id: string }> => {
  return {
    _id: 'any_id',
    uuid: generate_uuid(),
    name: 'any_name',
    email: 'any_email',
    password: 'any_password',
    age: 100,
    address: {
      raw: 'any_address',
    },
  }
}
describe('MongoUserRepository', () => {
  beforeEach(async () => {
    await MongoHelper.connect(process.env.MONGO_URL)
    const { _id, ...user } = await makeFakeUser()
    const collection = await classSut.collection()
    await collection.deleteMany({})
    await collection.insertOne(user)
  })

  afterEach(async () => {
    await MongoHelper.disconnect()
  })

  test('Check static parse_user', async () => {
    const user = await makeFakeUser()
    const { _id, ...expected } = user
    expect(classSut.parse_user(user)).toEqual({ id: _id, ...expected })
  })

  test('Must store user on mongoDB', async () => {
    const sut = await makeSut()
    const collection = await classSut.collection()

    await collection.deleteMany({})
    const user = await makeFakeUser()
    const { _id, password, ...user_data } = user

    const response = await sut.store({ ...user_data, password })
    expect(response).toEqual(expect.objectContaining(user_data))

    await expect(collection.countDocuments()).resolves.toEqual(1)
  })

  test('Must delete user on mongoDB', async () => {
    const sut = await makeSut()
    const collection = await classSut.collection()
    const user = classSut.parse_user(await collection.findOne({}))

    await sut.delete(user.uuid)

    await expect(collection.countDocuments()).resolves.toEqual(0)
  })

  test('Must list users on mongoDB', async () => {
    const sut = await makeSut()
    const collection = await classSut.collection()

    const db_users = await collection.find({}).toArray()
    const response = await sut.list()

    const parsed_users = db_users.map(classSut.parse_user)

    expect(response).toEqual(parsed_users)

    await expect(collection.countDocuments()).resolves.toEqual(1)
  })

  test('Must update user on mongoDB', async () => {
    const sut = await makeSut()
    const { _id: _, uuid: __, ...new_user_data } = await makeFakeUser()
    const collection = await classSut.collection()
    const { id, uuid } = classSut.parse_user(await collection.findOne({}))

    const response = await sut.update(uuid, new_user_data)

    await expect(response).toEqual({
      id,
      uuid,
      ...new_user_data,
    })
  })
})
